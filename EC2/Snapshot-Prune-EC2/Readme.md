This lambda function will search for all EC2 instances in all regions with tag named 'backup' with value 'true'.
For all EC2 instances with that tag, this function will take a snapshot of a instance.
