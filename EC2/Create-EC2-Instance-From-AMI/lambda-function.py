import os
import boto3

AMI = 'ami-0ba6fe7d36f6b81c8'
INSTANCE_TYPE = 't3.medium'
KEY_NAME = 'mgeweb'
SUBNET_ID = 'subnet-0e4e0556'
SECURITY_GROUP_ID = 'sg-099104e14684a2cae'
init_script = """#!/bin/bash
service ntpd stop
date 100212122019
nohup /home/ec2-user/dpa/dpa_12_1_701/startup.sh &"""



ec2 = boto3.resource('ec2')


def lambda_handler(event, context):

    instance = ec2.create_instances(
        ImageId=AMI,
        InstanceType=INSTANCE_TYPE,
        KeyName=KEY_NAME,
        SubnetId=SUBNET_ID,
        SecurityGroupIds= ["sg-099104e14684a2cae"],
        MaxCount=1,
        MinCount=1,
        UserData=init_script
    )

    print("New instance created:", instance[0].id)
