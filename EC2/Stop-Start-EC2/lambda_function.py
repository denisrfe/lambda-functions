import boto3
region = 'sa-east-1'
# Define the instance IDs. ex: ['i-09342df46b5f5cd6b', 'i-09342d446b5f5cd6b']
instances = ['i-09342df46b5f5cd6b']

def lambda_handler(event, context):
    ec2 = boto3.client('ec2', region_name=region)
    ec2.stop_instances(InstanceIds=instances)
    print ('stopped your instances: ', instances)