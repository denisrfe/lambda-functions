from datetime import date, datetime
import json
import boto3
from botocore.exceptions import ClientError
import os

ddbRegion = 'sa-east-1'

now = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")

def lambda_handler(event, context):
    try:
        ddb = boto3.client('dynamodb', region_name=ddbRegion)
        tables = ddb.list_tables()['TableNames']
        
        for table in tables:
            backupName = table + "-" + now
            ddb.create_backup(TableName=table,BackupName=backupName)
        print('Backup has been taken successfully for table:', table)

    except ClientError as e:
        print(e)

    except ValueError as ve:
        print('error:',ve)
    
    except Exception as ex:
        print(ex)
